
from mediamanager import MovieCleaning, Config

config = Config.load()

MOVIES_STAGE = config['movies']['MOVIES_STAGE']
MOVIES_ROOT = config['movies']['MOVIES_ROOT']

if __name__ == '__main__':
    MovieCleaning.clean_movie_files(MOVIES_STAGE, MOVIES_ROOT)
