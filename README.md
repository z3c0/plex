# Media Manager

## Using the scripts

To use this library, create a .cfg file at the root of the project, like so:

```cfg
[movies]
MOVIES_STAGE=/media/share/.stage/movies
MOVIES_ROOT=/media/share/movies

[tv]
TV_STAGE=/media/share/.stage/tv
TV_ROOT=/media/share/tv
```

The default config file name is `media.cfg`. This can be modified in the `Constants` class of the `mediamanager/subcomponents.py` file.

To process movie files, run `python clean_movies.py` or alternatively `cleanmovies` via bash.

To process tv files, run `python clean_tv.py` or alternatively `cleantv` via bash

***

Using `clean_tv.py` will loop over the `TV_STAGE` and move each folder to the `TV_ROOT`. Folder names are copied verbatim, so be sure to name the folder on the source system the way that they should appear on the target system.

Each file will have it's name scanned for a season number, an episode number, and a part number. Additional details will be dropped, and the file will be renamed in the `s00/s00e00` format, eg `tv_show_name/s01/s01e01.mp4`. Subtitle files will be copied as well.

### Cleaned TV Show Example

```cmd
mr_robot/
├── s01
│   ├── s01e01.mkv
│   ├── s01e02.mkv
│   ├── s01e03.mkv
│   ├── s01e04.mkv
│   ├── s01e05.mkv
│   ├── s01e06.mkv
│   ├── s01e07.mkv
│   ├── s01e08.mkv
│   ├── s01e09.mkv
│   └── s01e10.mkv
├── s02
│   ├── s02e01.mkv
│   ├── s02e02.mkv
│   ├── s02e03.mkv
│   ├── s02e04.mkv
│   ├── s02e05.mp4
│   ├── s02e06.mkv
│   ├── s02e07.mkv
│   ├── s02e08.mkv
│   ├── s02e09.mkv
│   ├── s02e10.mp4
│   ├── s02e11.mkv
│   └── s02e12.mkv
├── s03
│   ├── s03e01.mkv
│   ├── s03e02.mkv
│   ├── s03e03.mkv
│   ├── s03e04.mkv
│   ├── s03e05.mkv
│   ├── s03e06.mkv
│   ├── s03e07.mkv
│   ├── s03e08.mkv
│   ├── s03e09.mkv
│   └── s03e10.mkv
└── s04
    ├── s04e01.mp4
    ├── s04e02.mp4
    ├── s04e03.mp4
    ├── s04e04.mp4
    ├── s04e05.mp4
    ├── s04e06.mp4
    ├── s04e07.mp4
    ├── s04e08.mp4
    ├── s04e09.mp4
    ├── s04e10.mp4
    ├── s04e11.mp4
    ├── s04e12.mp4
    └── s04e13.mp4
```

***

Using `clean_movies.py` will loop over the `src_path` to find individual movie files. Each file will be scanned for a name and a year, and converted to an easily-parsed format, eg `this_is_a_movie_name_(2022).mp4`. Matching subtitles will be copied as well.

### Cleaned Movies Example

```bash
├── scott_pilgrim_vs_the_world_(2010).mkv
├── scream_(1996).eng.srt
├── scream_(1996).mkv
├── scream_(1996).mp4
├── scrooged_(1988).mp4
├── se7en_(1995).eng.srt
├── se7en_(1995).mp4
├── shaun_of_the_dead_(2004).mkv
├── shin_godzilla_(2016).eng.forced.srt
├── shin_godzilla_(2016).eng.srt
├── shin_godzilla_(2016).mp4
├── shiva_baby_(2021).mkv
├── shoot_em_up_(2007).mkv
├── shrek_(2001).eng.srt
├── shrek_(2001).mp4
├── shrek_2_(2004).eng.srt
├── shrek_2_(2004).mp4
├── shutter_island_(2010).eng.srt
├── shutter_island_(2010).mkv
├── sin_city_unrated_extended_(2005).mp4
├── skyfall_(2012).mp4
├── sleeping_beauty_(1959).mp4
├── snowpiercer_(2013).mp4
├── solaris_(1972).mp4
├── some_guy_who_kills_people_(2011).mp4
├── song_of_the_south_(1946).mkv
├── sonic_the_hedgehog_(2020).mkv
├── soul_(2020).mkv
├── south_park_post_covid_(2021).mkv
├── south_park_post_covid_the_return_of_covid_(2021).mkv
├── soylent_green_(1973).mkv
├── spaceballs_(1987).mkv
├── spectre_(2015).mp4
├── spider-man_(2002).mp4
├── spider-man_2_(2004).mkv
├── spider-man_far_from_home_(2019).mp4
├── spider-man_homecoming_(2017).mp4
├── spider-man_into_the_spider_verse_(2018).mkv
├── spider-man_no_way_home_(2021).mkv
├── spirited_away_(2001).mp4
```