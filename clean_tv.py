
from mediamanager import TeeVeeCleaning, Config

config = Config.load()

TV_STAGE = config['tv']['TV_STAGE']
TV_ROOT = config['tv']['TV_ROOT']

if __name__ == '__main__':
    TeeVeeCleaning.clean_tv_files(TV_STAGE, TV_ROOT)
