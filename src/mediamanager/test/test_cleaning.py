import re
import os
import sys
import shutil as sh
import unittest
import importlib.util

from enum import Enum
from types import ModuleType


class Submodule(Enum):
    MEDIAMANAGER = 'mediamanager', './src/mediamanager/__init__.py'


def prepare_submodule(submodule: Submodule) -> ModuleType:
    submodule_name, submodule_path = submodule.value
    spec = importlib.util.spec_from_file_location(submodule_name, submodule_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[submodule_name] = module
    spec.loader.exec_module(module)

    return module


class TestFileCleaning(unittest.TestCase):
    
    @classmethod
    def _prepare_test_files(cls, source, target, test_paths):
        source_directory = os.path.join('.test', re.sub(r'^/', '', source))
        target_directory = os.path.join('.test', re.sub(r'^/', '', target))
        
        os.makedirs(source_directory, exist_ok=True)
        os.makedirs(target_directory, exist_ok=True)
        
        # prepare landing zone
        for test_in, _ in test_paths:
            split_path = test_in.split('/')
            file_root = '/'.join(split_path[1:-1])
            file_name = split_path[-1]

            os.makedirs(os.path.join('.test', file_root), exist_ok=True)
            
            with open(os.path.join('.test', file_root, file_name), 'wb+') as sample_file:
                sample_file.write(b'1' * 1000000)            

        return source_directory, target_directory

    @staticmethod
    def _clean_test_files():
        try:
            sh.rmtree('.test')
        except FileNotFoundError:
            pass


class TestTeeVeeCleaning(TestFileCleaning):

    mediamanager = prepare_submodule(Submodule.MEDIAMANAGER)

    config = mediamanager.Config.load()

    TV_STAGE = config['tv']['TV_STAGE']
    TV_ROOT = config['tv']['TV_ROOT']

    test_tv_shows = [(f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E01.The.Gang.Inflates.1080p.AMZN.WEB-DL.DDP5.1.H.264-FLUX.mkv', 
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e01.mkv'),

                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E02.Frank.Shoots.Every.Member.of.the.Gang.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e02.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E03.The.Gang.Gets.Cursed.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e03.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E04.Frank.vs.Russia.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e04.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E05.Celebrity.Booze.The.Ultimate.Cash.Grab.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e05.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E06.Risk.E.Rats.Pizza.and.Amusement.Center.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e06.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E07.The.Gang.Goes.Bowling.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e07.mkv'),
                       
                     (f'{TV_STAGE}/its_always_sunny_in_philadelphia'
                        f'/Its.Always.Sunny.in.Philadelphia.S16.COMPLETE.1080p.AMZN.WEB-DL.DDP5.1.H.264-MIXED[TGx]'
                        f'/Its.Always.Sunny.in.Philadelphia.S16E08.Dennis.Takes.a.Mental.Health.Day.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb.mkv',
                      f'{TV_ROOT}/its_always_sunny_in_philadelphia/s16/s16e08.mkv')]
    
    def test_cleaning_tv_episode(self):
        for test_in, test_out in self.test_tv_shows:
            test_in_clean = self.mediamanager.TeeVeeCleaning.scrub_tv_stage(test_in, self.TV_STAGE)
            self.assertEqual(self.mediamanager.TeeVeeCleaning.clean_tv_file_name(test_in_clean, self.TV_ROOT), test_out)

    def test_clean_tv_files(self):

        self._clean_test_files()
        source_directory, target_directory = self._prepare_test_files(self.TV_STAGE, self.TV_ROOT, self.test_tv_shows)

        mv_function = self.mediamanager.TeeVeeCleaning.clean_tv_files

        mv_function(source_directory, target_directory)
        mv_function(source_directory, target_directory)

        for _, test_out in self.test_tv_shows:
            test_file_path = os.path.join('.test', re.sub(r'^/', '', test_out))

            try:
                file = open(test_file_path, 'rb')
            except FileNotFoundError:
                file = None
            finally:
                self.assertIsNotNone(file, f'NOT FOUND - {test_file_path}')
                file.close()


class TestMovieCleaning(TestFileCleaning):

    mediamanager = prepare_submodule(Submodule.MEDIAMANAGER)

    config = mediamanager.Config.load()
        
    MOVIES_STAGE = config['movies']['MOVIES_STAGE']
    MOVIES_ROOT = config['movies']['MOVIES_ROOT']

    test_movies = [(f'{MOVIES_STAGE}/Alanis (2017) [1080p] [WEBRip] [5.1] [YTS.MX]/Alanis.2017.1080p.WEBRip.x264.AAC5.1-[YTS.MX].mp4',
                    f'{MOVIES_ROOT}/alanis_(2017).mp4'),

                   (f'{MOVIES_STAGE}/Brightburn (2019) [WEBRip] [1080p] [YTS.LT]/Brightburn.2019.1080p.WEBRip.x264-[YTS.LT].mp4',
                    f'{MOVIES_ROOT}/brightburn_(2019).mp4'),
                    
                   (f'{MOVIES_STAGE}/Hell House LLC (2015) [WEBRip] [1080p] [YTS.AM]/Hell.House.LLC.2015.1080p.WEBRip.x264-[YTS.AM].mp4',
                    f'{MOVIES_ROOT}/hell_house_llc_(2015).mp4'),
                    
                   (f'{MOVIES_STAGE}/Lords Of Chaos (2018) [WEBRip] [1080p] [YTS.AM]/Lords.Of.Chaos.2018.1080p.WEBRip.x264-[YTS.AM].mp4',
                    f'{MOVIES_ROOT}/lords_of_chaos_(2018).mp4'),
                    
                   (f'{MOVIES_STAGE}/Martin (1978) [BluRay] [1080p] [YTS.AM]/Martin.1978.1080p.BluRay.x264-[YTS.AM].mp4',
                    f'{MOVIES_ROOT}/martin_(1978).mp4'),
                    
                   (f'{MOVIES_STAGE}/The House Of The Devil (2009) [BluRay] [1080p] [YTS.AM]/The.House.Of.The.Devil.2009.1080p.BluRay.x264-[YTS.AM].mp4',
                    f'{MOVIES_ROOT}/the_house_of_the_devil_(2009).mp4'),
                    
                   (f'{MOVIES_STAGE}/When Evil Lurks (2023) [1080p] [WEBRip] [x265] [10bit] [5.1] [YTS.MX]/When.Evil.Lurks.2023.1080p.WEBRip.x265.10bit.AAC5.1-[YTS.MX].mp4',
                    f'{MOVIES_ROOT}/when_evil_lurks_(2023).mp4'),
                    
                   (f'{MOVIES_STAGE}/2001.A.Space.Odyssey.1968.REMASTERED.720p.BluRay.999MB.HQ.x265.10bit-GalaxyRG[TGx]/2001.A.Space.Odyssey.1968.REMASTERED.720p.BluRay.999MB.HQ.x265.10bit-GalaxyRG.mkv',
                    f'{MOVIES_ROOT}/2001_a_space_odyssey_(1968).mkv'),
                    
                   (f'{MOVIES_STAGE}/Blade Runner 2049 (2017) [1080p] [YTS.AG]/Blade.Runner.2049.2017.1080p.BluRay.x264-[YTS.AG].mp4',
                    f'{MOVIES_ROOT}/blade_runner_2049_(2017).mp4'),
                   
                   (f'{MOVIES_STAGE}/The Occupation of the American Mind - Israel\'s Public Relations War in the United States (2016) 1080p Documentary.mp4',
                    f'{MOVIES_ROOT}/the_occupation_of_the_american_mind_israels_public_relations_war_in_the_united_states_(2016).mp4'),
                   
                   (f'{MOVIES_STAGE}/Escape To The Silver Globe (2021) [1080p] [WEBRip] [5.1] [YTS.MX]/Escape.To.The.Silver.Globe.2021.1080p.WEBRip.x264.AAC5.1-[YTS.MX].mp4',
                    f'{MOVIES_ROOT}/escape_to_the_silver_globe_(2021).mp4'),
                    
                   (f'{MOVIES_STAGE}/Howling II - Your Sister Is a Werewolf (1985) Philippe Mora.mkv',
                    f'{MOVIES_ROOT}/howling_ii_your_sister_is_a_werewolf_(1985).mkv'),
                    
                   (f'{MOVIES_STAGE}/Shin.Kamen.Rider.2023.1080p.AMZN.WEBRip.AAC5.1.10bits.x265-Rapta.mkv',
                    f'{MOVIES_ROOT}/shin_kamen_rider_(2023).mkv'),
                    
                   (f'{MOVIES_STAGE}/Leave.the.World.Behind.2023.1080p.NF.WEBRip.1600MB.DD5.1.x264-GalaxyRG.mkv',
                    f'{MOVIES_ROOT}/leave_the_world_behind_(2023).mkv'),
                    
                   (f'{MOVIES_STAGE}/Possessor.2020.UNCUT.1080p.WEBRip.x265-RARBG.mp4',
                    f'{MOVIES_ROOT}/possessor_(2020).mp4'),
                    
                   (f'{MOVIES_STAGE}/Ghost.World.2001.1080p.BluRay.x264.YIFY.mp4',
                    f'{MOVIES_ROOT}/ghost_world_(2001).mp4')]

    test_subtitles = [(f'{MOVIES_STAGE}/Alanis (2017) [1080p] [WEBRip] [5.1] [YTS.MX]/Alanis.2017.1080p.WEBRip.x264.AAC5.1-[YTS.MX].srt',
                       f'{MOVIES_ROOT}/alanis_(2017).eng.srt'),

                      (f'{MOVIES_STAGE}/When Evil Lurks (2023) [1080p] [WEBRip] [x265] [10bit] [5.1] [YTS.MX]/When.Evil.Lurks.2023.1080p.WEBRip.x265.10bit.AAC5.1-[YTS.MX].srt',
                       f'{MOVIES_ROOT}/when_evil_lurks_(2023).eng.srt'),
                       
                      (f'{MOVIES_STAGE}/Escape To The Silver Globe (2021) [1080p] [WEBRip] [5.1] [YTS.MX]/Escape.To.The.Silver.Globe.2021.1080p.WEBRip.x264.AAC5.1-[YTS.MX].srt',
                       f'{MOVIES_ROOT}/escape_to_the_silver_globe_(2021).eng.srt')]

    def test_cleaning_movies(self):
        for test_in, test_out in self.test_movies:
            self.assertEqual(self.mediamanager.MovieCleaning.clean_movie_file_name(test_in, self.MOVIES_ROOT), test_out)

    def test_cleaning_subtitles(self):
        for test_in, test_out in self.test_subtitles:
            self.assertEqual(self.mediamanager.MovieCleaning.clean_subtitle_file_name(test_in, self.MOVIES_ROOT), test_out)

    def test_clean_movie_files(self):
        self._clean_test_files()
        source_directory, target_directory = self._prepare_test_files(self.MOVIES_STAGE, self.MOVIES_ROOT, self.test_movies + self.test_subtitles)

        mv_function = self.mediamanager.MovieCleaning.clean_movie_files

        mv_function(source_directory, target_directory)
        mv_function(source_directory, target_directory)

        for _, test_out in self.test_movies + self.test_subtitles:
            test_file_path = os.path.join('.test', re.sub(r'^/', '', test_out))

            try:
                file = open(test_file_path, 'rb')
            except FileNotFoundError:
                file = None
            finally:
                self.assertIsNotNone(file, f'NOT FOUND - {test_file_path}')
                file.close()


if __name__ == '__main__':
    unittest.main(argv=[''], verbosity=2)
