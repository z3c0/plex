import os
import re
import glob
import shutil as sh

from .util import Constants


class FileCleaning:    
    @staticmethod
    def _is_video(path: str):
        extension = path.split('.')[-1]
        video_extensions = Constants.PREFERRED_VIDEO_EXTENSIONS.union(Constants.OTHER_VIDEO_EXTENSIONS)
        return extension in video_extensions

    @staticmethod
    def _is_subtitle(path: str):
        extension = path.split('.')[-1]
        return extension in Constants.SUBTITLE_EXTENSIONS


class TeeVeeCleaning(FileCleaning):
    @staticmethod
    def _clean_file_name(path: str) -> tuple[str, str, str]:
        series, *subfolder_hierarchy = path.split('/')
        file_name = subfolder_hierarchy[-1]
        extension = file_name.split('.')[-1]

        episode_match = re.search(Constants.Tv.EPISODE_REGEX, file_name).groupdict()
    
        season = episode_match['season_num']
        episode = episode_match['episode_num']
        episode_ext = episode_match.get('episode_num_ext')
        part = episode_match.get('part_num')

        cleaned_name = f's{season}/s{season}e{episode}'
        
        if episode_ext:
            cleaned_name += f'-e{episode}'
        
        if part:
            cleaned_name += f'-part{part}'

        return series, cleaned_name, extension
    
    @staticmethod
    def scrub_tv_stage(file_path: str, stage_path: str) -> str:
        return re.sub(fr'^{stage_path}/', '', file_path)

    @classmethod
    def clean_tv_file_name(cls, path: str, target_directory: str) -> str:
        series, episode, extension = cls._clean_file_name(path)

        cleaned_name = f'{series}/{episode}.{extension}'

        return os.path.join(target_directory,  cleaned_name)
    
    @classmethod
    def clean_subtitle_file_name(cls, path: str, target_directory: str) -> str:
        series, episode, extension = cls._clean_file_name(path)

        cleaned_name = f'{series}/{episode}.eng.{extension}'

        return os.path.join(target_directory,  cleaned_name)
    
    @classmethod
    def clean_tv_files(cls, source_directory: str, target_directory: str) -> str:
        all_files = glob.glob(os.path.join(source_directory, '**/*'), 
                              recursive=True, include_hidden=True)

        total_files = len(all_files)
        placeholders = len(str(total_files))
        file_count = 0

        for source_file in sorted(all_files):
            if os.path.isdir(source_file):
                continue

            elif cls._is_video(source_file):
                source_file_clean = cls.scrub_tv_stage(source_file, source_directory)
                target_file = cls.clean_tv_file_name(source_file_clean, target_directory)

            elif cls._is_subtitle(source_file):
                source_file_clean = cls.scrub_tv_stage(source_file, source_directory)
                target_file = cls.clean_subtitle_file_name(source_file_clean, target_directory)

            else:
                continue

            file_count += 1

            if os.path.isfile(target_file):
                print(f'{"-" * placeholders}', target_file)
                continue

            print(f'{str(file_count).zfill(placeholders)}', target_file)
            os.makedirs('/'.join(target_file.split('/')[:-1]), exist_ok=True)
            sh.copy2(source_file, target_file)


class MovieCleaning(FileCleaning):
    @staticmethod
    def _clean_file_name(path: str) -> tuple[str, str, str]:
        file_name = path.split('/')[-1].lower()

        for token in Constants.Movies.COMMON_TOKENS:
            file_name = re.sub(fr'\b{token}\b', '', file_name)

        name_match = re.match(Constants.Movies.NAME_REGEX_PATTERN, file_name)
        title = name_match.group(1)
        year = name_match.group(2)
        extension = file_name.split('.')[-1]

        title = re.sub(r'[\']', '', title)
        title = re.sub(r'\W', '_', title)
        title = re.sub(r'_+$', '', title)

        while '__' in title:
            title = title.replace('__', '_')

        return title, year, extension

    @classmethod
    def clean_movie_file_name(cls, path: str, target_directory: str) -> str:
        title, year, extension = cls._clean_file_name(path)

        cleaned_name = f'{title}_({year}).{extension}'

        return os.path.join(target_directory,  cleaned_name)
    
    @classmethod
    def clean_subtitle_file_name(cls, path: str, target_directory: str) -> str:
        title, year, extension = cls._clean_file_name(path)

        cleaned_name = f'{title}_({year}).eng.{extension}'

        return os.path.join(target_directory,  cleaned_name)


    @classmethod
    def clean_movie_files(cls, source_directory: str, target_directory: str):
        all_files = glob.glob(os.path.join(source_directory, '**/*'), 
                              recursive=True, include_hidden=True)
        
        total_files = len(all_files)
        placeholders = len(str(total_files))
        file_count = 0

        for source_file in sorted(all_files):
            if os.path.isdir(source_file):
                continue

            elif cls._is_video(source_file):
                target_file = cls.clean_movie_file_name(source_file, target_directory)

            elif cls._is_subtitle(source_file):
                try:
                    target_file = cls.clean_subtitle_file_name(source_file, target_directory)
                except AttributeError:
                    continue
            else:
                continue

            file_count += 1

            if os.path.isfile(target_file):
                print(f'{"-" * placeholders}', target_file)
                continue

            print(f'{str(file_count).zfill(placeholders)}', target_file)
            sh.copy2(source_file, target_file)
