import os
from configparser import ConfigParser


class Config:
    @staticmethod
    def load():
        config = ConfigParser()

        if os.path.exists('./media.cfg'):
            config.read('./media.cfg')
        elif os.path.exists('~/media.cfg'):
            config.read('~/media.cfg')
        elif os.path.exists('/usr/media.cfg'):
            config.read('/usr/media.cfg')
        else:
            raise FileNotFoundError('media.cfg not found in home, usr, or working directory')
        
        return config


class Constants:
    PREFERRED_VIDEO_EXTENSIONS = {'mkv', 'mp4'}

    OTHER_VIDEO_EXTENSIONS = {'flv', 'f4p', 'ogv', 'asf', 'amv', 'mpg',
                              'f4b', 'yuv', 'nsv', 'svi', 'mov', 'f4v',
                              'qt', '3gp', 'mxf', 'mp2', 'gif', 'roq',
                              'drc', 'gifv', 'mpe', 'rm', 'wmv', 'webm',
                              'mpeg', 'ogg', 'm2v', 'mng', 'm2ts', 'mts',
                              'avi', 'rmvb', 'vob', 'm4v'}

    SUBTITLE_EXTENSIONS = {'srt', 'idx', 'sub'}

    @property
    @classmethod
    def video_extensions(cls):
        return cls.PREFERRED_VIDEO_EXTENSIONS.union(cls.OTHER_VIDEO_EXTENSIONS)

    class Tv:
        EPISODE_REGEX = (r'(?P<episode>'  # start episode group

                         # eg s01 or S01 or 1
                         r'(?P<season>[sS]?(?P<season_num>\d+))?'

                         # episode number: e01 or E01 or x01
                         # optionally, preceding "_", "-", or " "
                         r'[\-_ ]?[xeE](?P<episode_num>\d+)'

                         # detects multi-episode file
                         # (-e02, -E02, ,-x02, -02)
                         r'-?[xeE]?(?P<episode_num_ext>\d+)?'

                         # close episode group
                         r')'

                         # detects multi-part episode
                         r'(?P<part_num>-(pt|part)\d)?')

    class Movies:
        NAME_REGEX_PATTERN = r'^(?P<title>[a-zA-Z0-9 \.\'\-!_&\(\)]+)(?P<year>(19|20)\d{2}).+$'
        VALID_NAME_PATTERN = r'[^a-z0-9\._]'

        COMMON_TOKENS = {'web', 'bluray', 'dvdrip', '2160p', '1080p', '720p',
                         '4k', 'hd', 'x264', 'x265', '5.1'}
